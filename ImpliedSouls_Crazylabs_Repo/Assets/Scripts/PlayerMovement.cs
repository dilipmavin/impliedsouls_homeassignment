using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class PlayerMovement : MonoBehaviour
{

    public UIManager uManager;
    public BallManager bManager;
    private CharacterController controller;
    private Vector3 direction;
    public float forwardSpeed;
    private int desiredLane=1;
    public float laneDistance = 4;

    public SneakerManager sManager;

    public GameObject Bricks_Holder;


    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    void Update()
    {
        direction.z = forwardSpeed;

        if(SwipeManager.swiperight)
        {
            desiredLane++;
            if(desiredLane==3)
            {
                desiredLane = 2;
            }
        }
        if (SwipeManager.swipeleft)
        {
            desiredLane--;
            if (desiredLane == -1)
            {
                desiredLane = 0;
            }
        }

        Vector3 targetPosition = transform.position.z * transform.forward + transform.position.y * transform.up;

        if (desiredLane == 0)
        {
            targetPosition += Vector3.left * laneDistance;
        }
        else if (desiredLane == 2)
        {
            targetPosition += Vector3.right * laneDistance;
        }

       
        transform.position = Vector3.Lerp(transform.position,targetPosition,10*Time.deltaTime);
        controller.center = controller.center;
    }

    private void FixedUpdate()
    {
        Physics.SyncTransforms();
        controller.Move(direction * Time.fixedDeltaTime);
    }


    private void OnTriggerEnter(Collider other)
    {     
        if (other.tag == "Sneaker")
        {
            sManager.IncreaseSneakerSize();
            other.gameObject.SetActive(false);
            uManager.InncreaseCount();
            Debug.Log("----increae size-----");
        }

        if (other.tag == "Enemy")
        {
            other.gameObject.GetComponent<BoxCollider>().enabled = false;
            if(sManager.Rightfoot.localScale.x>=1)
            {

               forwardSpeed = 0;
               this.GetComponent<Animator>().Play("Attack");
                StartCoroutine(SlowDown(other.gameObject.transform));
              //other.gameObject.SetActive(false);

            }
            else
            {
                forwardSpeed = 0;
                other.gameObject.GetComponent<Animator>().Play("Attack");
                StartCoroutine(GameOver());
            }
        }

        if (other.tag == "SneakerObstacle")
        {
            uManager.DecreaseCount();

            sManager.DecreaseSneakerSize();
            this.gameObject.GetComponent<Animator>().Play("Edge");
            forwardSpeed = 5;
            StartCoroutine("ResetSpeed");
           // other.gameObject.SetActive(false);
        }

        if (other.tag == "Finish")
        {
            SwipeManager.canSwipe = false;
            other.gameObject.GetComponent<BoxCollider>().enabled = false; 
            desiredLane = 1;
        }

        if (other.tag == "Kick")
        {
            forwardSpeed = 0;
            this.gameObject.GetComponent<Animator>().Play("Kick");
            StartCoroutine("Ball");
        }

        if (other.tag == "Dead")
        {
            GameOver();
        }
    }

    public IEnumerator ResetSpeed()
    {
        yield return new WaitForSeconds(1f);
        forwardSpeed = 10;
    }
    public IEnumerator Ball()
    {
        yield return new WaitForSeconds(0.4f);
        bManager.gameObject.transform.parent = null;
        Camera.main.GetComponent<CameraController>().player = bManager.Ball.gameObject;

        if(uManager.Counter==1)
        {
            bManager.MoveToDestination(129);

        }
        else if (uManager.Counter == 2)
        {
            bManager.MoveToDestination(131);

        }
        else if (uManager.Counter == 3)
        {
            bManager.MoveToDestination(134);

        }
        else if (uManager.Counter == 4)
        {
            bManager.MoveToDestination(136);

        }
        else if (uManager.Counter == 5)
        {
            bManager.MoveToDestination(139);

        }

        else if (uManager.Counter == 6)
        {
            bManager.MoveToDestination(142);

        }
        else if (uManager.Counter == 7)
        {
            bManager.MoveToDestination(145);

        }
        else if (uManager.Counter == 8)
        {
            bManager.MoveToDestination(147);

        }
        else if (uManager.Counter == 9)
        {
            bManager.MoveToDestination(150);

        }
        else if (uManager.Counter == 10)
        {
            bManager.MoveToDestination(153);
        }

        yield return new WaitForSeconds(5.2f);
        uManager.GiveReward();

    }

    public IEnumerator SlowDown(Transform obj)
    {
        yield return new WaitForSeconds(0.5f);
        obj.DOMoveY(15, 3);
        yield return new WaitForSeconds(0.5f);
        this.gameObject.GetComponent<Animator>().Play("walk");


        forwardSpeed = 8;
    }

    public IEnumerator GameOver()
    {
        yield return new WaitForSeconds(0.5f);
        this.GetComponent<Animator>().Play("Dead");
        forwardSpeed = 0;
      //  this.GetComponent<Animator>().Play("Idle");
      //  GameObject.Find("Canvas").GetComponent<UIManager>().LooseScreen();
        Debug.Log("-----Dead-----");
    }

}