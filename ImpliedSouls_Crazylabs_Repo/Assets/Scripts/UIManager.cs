using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    public static int StartCount;
    public int Counter;
    public GameObject MainMenuItens;
    public GameObject Tutorial;
    public PlayerMovement pMovement;
    public GameObject[] shoes;
    public TextMeshProUGUI countText;
    public GameObject RewardParent;
    public GameObject Hook;
    public GameObject Splash;

    public GameObject Menu2;
    // Start is called before the first frame update
    void Start()
    {
      
        if(StartCount==0)
        {
            StartCoroutine("PlayScreen");
        }
        else
        {
            Splash.SetActive(false);
            MainMenuItens.SetActive(true);
        }
        StartCount++;
    }


    public IEnumerator PlayScreen()
    {
        yield return new WaitForSeconds(1.4f);
        Splash.SetActive(false);
        MainMenuItens.SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {
    }

    public void TutorialScene()
    {
        MainMenuItens.SetActive(false);
        pMovement.forwardSpeed = 10;
        pMovement.gameObject.GetComponent<Animator>().Play("Walk");
        //   Tutorial.SetActive(true);
        SwipeManager.canSwipe = true;
    }
    public void Play()
    {
        Tutorial.SetActive(false);
    }

    public void InncreaseCount()
    {
        Counter++;
        countText.text = Counter.ToString();
        ChangeHookPosition();
    }

    public void DecreaseCount()
    {
        Counter--;
        countText.text = Counter.ToString();
        ChangeHookPosition();
    }


    public void GiveReward()
    {
        StartCoroutine("RewardFlow");
        RewardParent.SetActive(true);
    }

    public IEnumerator RewardFlow()
    {
        for(int i=0;i<shoes.Length;i++)
        {            
            shoes[i].transform.DOMoveX(568, 0.5f);
            shoes[i].transform.DOMoveY(1338, 0.5f);
            yield return new WaitForSeconds(0.15f);
            InncreaseCount();
        }

        yield return new WaitForSeconds(2);
        ShowFinalScreen();
    }

    public void ChangeHookPosition()
    {
        if (Counter == 1)
        {
            Hook.transform.DOLocalMoveY(30, 0.2f);
        }
        else if (Counter == 2)
        {
            Hook.transform.DOLocalMoveY(120, 0.2f);

        }
        else if (Counter == 3)
        {
            Hook.transform.DOLocalMoveY(220, 0.2f);

        }
        else if (Counter == 4)
        {
            Hook.transform.DOLocalMoveY(320, 0.2f);

        }
        else if (Counter == 5)
        {
            Hook.transform.DOLocalMoveY(350, 0.2f);

        }
        else if (Counter == 6)
        {
            Hook.transform.DOLocalMoveY(410, 0.2f);

        }
        else if (Counter == 7)
        {
            Hook.transform.DOLocalMoveY(470, 0.2f);

        }
        else if (Counter == 8)
        {
            Hook.transform.DOLocalMoveY(515, 0.2f);

        }
        else if (Counter == 9)
        {
            Hook.transform.DOLocalMoveY(550, 0.2f);

        }
        else if (Counter == 10)
        {
            Hook.transform.DOLocalMoveY(620, 0.2f);
        }

    }


    public void ShowFinalScreen()
    {
        Menu2.SetActive(true);
        SwipeManager.canSwipe = false;
    }
    public void Easy()
    {
        SceneManager.LoadScene("Easy");
    }

    public void Medium()
    {
        SceneManager.LoadScene("Medium");

    }

    public void Hard()
    {
        SceneManager.LoadScene("Hard");

    }

}
